// POW basic block mining

const SHA256 = require("crypto-js/sha256");

class Block {
    constructor(index, timestamp, data, previousHash = '') {
        this.index = index;
        this.previousHash = previousHash;       
        this.timestamp = timestamp;
        this.data = data;
        this.hash = this.calculateHash();
        this.nonce = 0;
    }

    calculateHash() {
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString(); 
    }

    mineBlock(difficulty) {
        while (this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")) {
            this.nonce++;
            console.log("nonce: " + this.nonce);
            this.hash = this.calculateHash();
            console.log("hash candidato: " + this.hash);
        }

        console.log("HASH BLOQUE MINADO: " + this.hash);
    }
}

class Blockchain {
    constructor() {
        this.chain = [this.createGenesisBlock()];
        this.difficulty = 1;
    }

    createGenesisBlock() {
        return new Block(0, Date.parse("2018-01-01"), "Genesis block", "0");
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    addBlock(newBlock) {
        newBlock.previousHash = this.getLatestBlock().hash;
        newBlock.mineBlock(this.difficulty);
        this.chain.push(newBlock);
    }

    isChainValid() {
        for (let i = 1; i < this.chain.length; i++){
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }

            if (JSON.stringify(this.chain[0]) !== JSON.stringify(this.createGenesisBlock())) {
                return false;
            }
        }

        return true;
    }
}

let basicBlockchain = new Blockchain();

console.log("----- la Blockchain inicial, incluye los bloques minados ----");
console.log(JSON.stringify(basicBlockchain, null, 4));

console.log("************"); 

console.log('----- Minando bloque 2 -----');
basicBlockchain.addBlock(new Block(1, Date.parse("2018-08-01"), { amount: 4 }));

console.log("************");

console.log('----- Minando bloque 3 -----');
basicBlockchain.addBlock(new Block(2, Date.parse("2018-09-01"), { amount: 8 }));

console.log("************"); 

console.log("----- la nueva Blockchain , incluye los bloques minados ----");
console.log(JSON.stringify(basicBlockchain, null, 4));

console.log("************"); 