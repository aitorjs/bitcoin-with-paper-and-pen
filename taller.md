# Bitcoin con papel y boli

## Guia
0. Presentacion mia y del indice del taller 3mins

1. Explicacion de Bitcoin.  40mins
    - Empezamos cronologicamente: 
        * 2008-10-31: Satoshi manda un email a una lista de criptografia https://satoshi.nakamotoinstitute.org/emails/
        * 2009-01-03: Inicia la cadena de bloques de Bitcoin con el bloque genesis: https://blockstream.info/block/000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f
        * 2009-02-11: Presenta Bitcoin en una lista llamada P2P Foundation https://satoshi.nakamotoinstitute.org/posts/

        * Que es lo que satoshi presenta?

          - Tecnicamente Bitcoin consiste en:
            - El protocolo Bitcoin (the Bitcoin protocol): Red peer-to-peer descentralizada 
            - La cadena de bloques (blockchain): Libro publico mayor de transacciones
            - Reglas de consenso (consensus rules): Un conjunto de reglas para la validacion independientede las transacciones y emision de la moneda
            - Protocolos de consenso (consensus protocols): Mecanimos para alcanzar un consenso global descentralizado en la cadena de bloques válida. Proof Of Work (PoW) para Bitcoin.


          - Bitcoin-ek teknikoki honako gauzak darakartza:
            - Bitcoin protokoloa (the Bitcoin protocol): Peer-to-peer (P2P) sare deszentralizatua
            - Bloke katea (blockchain): Transakzioen liburu publiko handia (Libro publico mayor de transacciones)
            - Adostasun arauak (consensus rules): Transazioak balidazio independientean eta moneta igotzeko arau multzoa.
            - Adostasun protokoloak (consensus protocols): Baliozko Bloke katean adostasun global deszentralizaty lortzeko mekanismoak. Bitcoin-aren kasuan, Proof of Work (PoW).


        - Empezamos desde un nodo.
        - Ponemos mas nodos
        - Como se haria una transaccion. https://gitlab.com/aitoribanez/childrenWallet/blob/master/esquemaCadenaBitcoin.pdf

-[ES] White paper Bitcoin https://nakamotoinstitute.org/static/docs/bitcoin-es.pdf
- Hacer peticiones RPC a un nodo de Bitcoin usando un navegador web: http://chainquery.com/bitcoin-api
- [ES] "Mastering Bitcoin" https://bitcoinbook.info/wp-content/translations/es/book.pdf
- Explicación visual de las hash functions con SHA256: https://anders.com/blockchain/hash.html

2. Ejemplo de pequeñas cadenas de bloques hechas en nodejs 25 mins
    - basicBLockchain1.js:(cadena de bloques con bloques con datos)  Crea una cadena de bloques, le añade dos bloques, los valida, le daria ok y cambia el amount, por lo que, cambia el hash del bloque2 dando no-valido.

- basicBLockchain2.js (añadimos PoW): Crea una cadena de bloques,  mina el bloque 1 y mina el bloque 2.

- basicBLockchain3.js (añadimos transacciones y mempool. sin direcciones bitcoin): Crea una cadena de bloques, crea 4 transacciones, pinta el balance del miner-address,  mina las transacciones pendientes y lo vuelve hacer por una segunda vez.

- blockchain.info.js: Hace peticiones a la API de blockchain.info. No es interesante para el taller.

3. Ejemplos de conexion a nodos de la red P2P de Bitcoin en pythin con jupyter y ver un poco el protocolo de red de Bitcoin 25mins

    - https://github.com/justinmoon/bitcoincorps/blob/master/1.%20Network%20Message%20Structure.ipynb
    - https://github.com/justinmoon/bitcoincorps/blob/master/2.%20Reading%20Version%20Messages.ipynb
    - https://github.com/justinmoon/bitcoincorps/blob/master/3.%20Executing%20the%20Version%20Handshake.ipynb
    
[EN] https://en.bitcoin.it/wiki/Protocol_documentation

4. Bibliografia 2mins

- "Mastering Bitcoin"
-  https://en.bitcoin.it/wiki

5. Preguntas! x mins